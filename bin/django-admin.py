#!/usr/bin/python2.4
# EASY-INSTALL-SCRIPT: 'Django==0.91','django-admin.py'
__requires__ = 'Django==0.91'
import pkg_resources
pkg_resources.run_script('Django==0.91', 'django-admin.py')
