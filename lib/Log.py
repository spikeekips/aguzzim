# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import logging, logging.handlers, sys, time
from twisted.python import logfile as log_twisted

class __DEFAULT__ :

	MSG_FORMAT = "%-20s %-10s : %s\n"

	LEVEL = 0
	TYPE = "rotate"
	MAXBYTE = 30000000
	DIRECTORY = "./"
	LEVEL = 0
	BACKUPCOUNT = 10

	def __init__ ( \
				self, name, directory="./", maxbyte=300000000, \
				level="notset", type="rotate", backupcount=10 \
			) :

		self.LEVEL = level
		self.TYPE = type
		self.MAXBYTE = maxbyte
		self.DIRECTORY = directory
		self.LEVEL = level
		self.BACKUPCOUNT = backupcount

	def write (self, type, msg) :
		sys.stdout.write(self.MSG_FORMAT % (time.strftime("%F %T"), type, msg, ))

	def __checkLevel (self, type) :
		__level = getattr(logging, type.upper())
		__level_now = getattr(logging, self.LEVEL.upper())
		if __level >=  __level_now : return True

		return False

	def error (self, msg) :
		if not self.__checkLevel("error") : return
		self.write("ERROR", msg)

	def critical (self, msg) :
		if not self.__checkLevel("critical") : return
		self.write("CRITICAL", msg)

	def warning (self, msg) :
		if not self.__checkLevel("warning") : return
		self.write("WARNING", msg)

	def info (self, msg) :
		if not self.__checkLevel("info") : return
		self.write("INFO", msg)

class Twisted (__DEFAULT__) :

	MSG_FORMAT = "%-20s %-10s : %s\n"

	def __init__ ( \
				self, name, directory="./", maxbyte=300000000, \
				level="notset", type="rotate", backupcount=10 \
			) :

		__DEFAULT__.__init__( \
			self, name, directory, maxbyte, \
			level, type, backupcount \
		)

		self.LOG = log_twisted.LogFile( \
			str(name), \
			directory, \
			maxbyte, \
			None \
		)

	def write (self, type, msg) :
		__msg = self.MSG_FORMAT % (time.strftime("%F %T"), type, msg, )

		self.LOG.write(__msg)
		self.LOG.flush()

class Classic (__DEFAULT__) :

	logger = None

	MSG_FORMAT = "%(asctime)s %(levelname)s %(message)s"
	ERROR_PERMISSION = "Can not create log file, %s\n"

	def __init__ ( \
				self, name, directory="./", maxbyte=300000000, \
				level="notset", type="rotate", backupcount=10 \
			) :

		__DEFAULT__.__init__( \
			self, name, directory, maxbyte, \
			level, type, backupcount \
		)

		logfile = "%s/%s.log" % (directory, name)
		self.logger = logging.getLogger(str(name))
		self.__set_handler(logfile)

		formatter = logging.Formatter(self.MSG_FORMAT)
		self.hdlr.setFormatter(formatter)

		self.logger.addHandler(self.hdlr)
		self.logger.setLevel(self.LEVEL)

		self.__info = self.logger.info
		self.__error = self.logger.error
		self.__warning = self.logger.warning
		self.__critical = self.logger.critical

	def write (self, type, msg) :
		__type = __type.lower()

		if __type == "info" :
			self.logger.info(msg)
		elif __type == "error" :
			self.logger.error(msg)
		elif __type == "warning" :
			self.logger.warning(msg)
		elif __type == "critical" :
			self.logger.critical(msg)

	def __set_handler (self, logfile) :
		if self.TYPE == "rotate" :
			try :
				self.hdlr = logging.handlers.RotatingFileHandler( \
					logfile, "a", self.maxbyte, self.backupcount)
			except IOError :
				raise IOError, self.ERROR_PERMISSION % logfile
		else :
			self.hdlr = logging.FileHandler(logfile, "a")

Default = Twisted

"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spike@spikeekips.net>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "Log.py"


