# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import os, urllib, sre
SRE_START_HEAD = sre.compile("<head", sre.I)
SRE_END_HEAD = sre.compile("</head>", sre.I)
SRE_START_LINK = sre.compile("<link ", sre.I)
SRE_START_HREF = sre.compile("href=", sre.I)
SRE_LINK_TYPE_RSS = sre.compile("TYPE=[\"']application/rss\+xml[\"']", sre.I)

def get_rss_path_in_page (url, content) :

	__o = content.split(os.linesep)

	__data_head = list()
	__entering_in_head = False
	for __d in __o :
		if not __entering_in_head :
			if SRE_START_HEAD.findall(__d) :
				__entering_in_head = True

		if __entering_in_head :
			__data_head.append(__d)

			if SRE_END_HEAD.findall(__d) :
				break

	__d = "".join(__data_head)
	__head = SRE_END_HEAD.split(SRE_START_HEAD.split(__d)[1])[0]

	__links = SRE_START_LINK.split(__head)
	__d = list()
	for i in __links :
		if SRE_LINK_TYPE_RSS.findall(i) :
			__colon = "\""

			__o = SRE_START_HREF.split(i)[1]
			if __o[0] == "\"" :
				__colon = "\""
			elif __o[0] == "'" :
				__colon = "'"

			__oo = list()
			for j in __o[1:] :
				if j == __colon : break
				__oo.append(j)

			__d.append("".join(__oo))

	__o = list()
	for i in __d :
		__o.append(urllib.basejoin(url, i))

	return __o


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "HTML.py"


