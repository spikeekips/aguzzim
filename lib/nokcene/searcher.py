"""
nokcene searcher
"""

import os
import shutil
import PyLucene

#import zope.interface
#from interfaces import ILuceneSearcher

class LuceneSearcher(object):
    """Lucene Searcher
    """

    #zope.interface.implements(ILuceneSearcher)

    _searcher = None

    def __init__(self, store_dir):
        self._store = PyLucene.FSDirectory.getDirectory(store_dir, False)
        try:
            self._searcher = self.get()
        except PyLucene.JavaError:
            # No indexes yet
            return None
        self.close()

    def get(self):
        if not self._searcher:
            self._searcher = PyLucene.IndexSearcher(self._store)
        return self._searcher

    def close(self):
        if self._searcher:
            self._searcher.close()
            self._searcher = None

    def __del__(self):
        self.close()
