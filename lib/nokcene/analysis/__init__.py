"""
Analyzer Helper
"""

import PyLucene

from sort import NXSortAnalyzer

analyzers_map = {
    # XXX not complete
    "standard"	: PyLucene.StandardAnalyzer(),
    "cjk"		: PyLucene.CJKAnalyzer(),
    "sort"		: NXSortAnalyzer(),
    "keyword"	: PyLucene.KeywordAnalyzer(),
    }

def getAnalyzerById(analyzer_id):
    if not analyzer_id.lower() in analyzers_map.keys():
        analyzer_id = "cjk"
    return analyzers_map.get(analyzer_id)

def getPerFieldAnalyzerWrapper(default_analyzer=None):
    if default_analyzer is None:
        default_analyzer = getAnalyzerById("cjk")

    return PyLucene.PerFieldAnalyzerWrapper(default_analyzer)

