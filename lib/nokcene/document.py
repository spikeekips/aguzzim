"""
Nokcene document.definition
"""

import PyLucene

UID_FIELD_ID = 'uid'

class Document(PyLucene.Document):

    def __init__(self, uid=None):

        if uid is None:
            raise ValueError("You need to provide an uid for this document")

        PyLucene.Document.__init__(self)
        self.add(PyLucene.Field(UID_FIELD_ID, unicode(uid), PyLucene.Field.Store.YES, PyLucene.Field.Index.TOKENIZED))

    def getUID(self):
        return self.get(UID_FIELD_ID)

        
