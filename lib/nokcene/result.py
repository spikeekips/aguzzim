# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import xmlrpclib
from nokcene.rss.resultset import ResultSet
from nokcene.rss.adapter import PythonResultSet
import func

import config

TEMPLATE_XMLQUERY_DOC = \
"""<?xml version="1.0" encoding="utf-8"?>
<search>%s
</search>
"""
TEMPLATE_XMLQUERY_FIELDS = \
"""
	<fields>%s
	</fields>"""
TEMPLATE_XMLQUERY_FIELD = \
"""
		<field id="%(id)s" value=%(value)s usage="%(usage)s" type="%(type)s"
			analyzer="cjk" condition="%(condition)s" />"""
TEMPLATE_XMLQUERY_RETURN_FIELDS = \
"""
	<return_fields>%s
	</return_fields>"""
TEMPLATE_XMLQUERY_RETURN_FIELD = \
"""
		<field>%s</field>"""

TEMPLATE_XMLQUERY_SEARCH_OPTIONS = \
"""
	<sort>
		<sort-on>%(sort-on)s</sort-on>
		<sort-order>%(sort-order)s</sort-order>
		<sort-limit>%(size)s</sort-limit>
		<operator>%(operator)s</operator>
	</sort>
	<batch start="%(start)s" size="%(size)s"/>"""

# create query xml for searchQuery(XMLRPC API)
def generate_xml_query \
		(return_fields=dict(), search_fields=dict(), search_options=dict()) :

	"""
	Examples :
	<?xml version="1.0" encoding="utf-8"?>
	<search>
		<fields>
			<field id="name" value="foo" />
		</fields>
		<return_fields>
			<field></field>
		</return_fields>
		<fields/>
		<sort>
			<sort-on>Title</sort-on>
			<sort-order>reverse</sort-order>
			<sort-limit>100</sort-limit>
		</sort>
		<batch start="10" size="100"/>
	</search>
	"""

	# return fields
	__list_return_fields = list()
	for i in return_fields :
		__list_return_fields.append(TEMPLATE_XMLQUERY_RETURN_FIELD % i)

	__elements_return_fields = TEMPLATE_XMLQUERY_RETURN_FIELDS % ("".join(__list_return_fields), )

	# fields
	__list_fields = list()
	for i in search_fields :
		i["value"] = func.quote_xml_attr(i["value"])
		__list_fields.append(TEMPLATE_XMLQUERY_FIELD % i)

	__elements_fields = TEMPLATE_XMLQUERY_FIELDS % ("".join(__list_fields), )

	# sort
	__elements_search_options = TEMPLATE_XMLQUERY_SEARCH_OPTIONS % search_options

	__doc_query = TEMPLATE_XMLQUERY_DOC % (__elements_return_fields + __elements_fields + __elements_search_options)

	return __doc_query

def generate_result (hit_xml) :
	rs = PythonResultSet(ResultSet(hit_xml))

	(result, num, ) = rs.getResults()

	return (result, num, )

def get_result (xml_query) :
	s = xmlrpclib.Server("http://localhost:%d" % config.nokcene_port)
	hit_xml = s.searchQuery(xml_query)
	return generate_result(hit_xml)

if __name__ == "__main__" :
	import sys

	return_fields=(
		"title",
		"summary",
		"timeUpdated",
		"author",
		"link",
		"link__",
		"__uid__",
	)
	search_fields=( \
		{
			"id" : "document_type",
			"type" : "keyword",
			"value": "channel_entry",
			"usage" : "",
			"condition" : "AND",
		},
		{
			"id" : "timeUpdated",
			"type" : "date",
			"value": "2006-08-23 08:00:00",
			"usage" : "range:min",
			"condition" : "AND",
		},
		{
			"id" : "timeUpdated",
			"type" : "date",
			"value": "2006-08-23 09:00:00",
			"usage" : "range:max",
			"condition" : "AND",
		},
	)
	search_options = { \
		"sort-on" : "",
		"sort-order" : "reverse",
		"start" : 0,
		"size" : 15,
		"operator" : "AND",
	}

	__xml = generate_xml_query( \
		return_fields=return_fields, \
		search_fields=search_fields, \
		search_options=search_options \
	)
	(result, items, ) = get_result(__xml)
	for i in result :
		print i.keys()
		#print i["link"]
		#print i
		pass


	sys.exit()


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "1.py"


