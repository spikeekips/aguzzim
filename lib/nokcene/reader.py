"""
nokcene reader
"""

import os
import shutil
import PyLucene

#import zope.interface
#from interfaces import ILuceneReader

class LuceneReader(object):
    """Lucene Reader
    """

    #zope.interface.implements(ILuceneReader)

    _reader = None

    def __init__(self, store_dir, creation=False):
        if creation:
            if os.path.exists(store_dir):
                shutil.rmtree(store_dir)
            os.makedirs(store_dir)
        self._store = PyLucene.FSDirectory.getDirectory(store_dir, creation)
        self._reader = self.get()
        self.close()

    def get(self):
        if not self._reader:
            self._reader = PyLucene.IndexReader.open(self._store)
        return self._reader

    def close(self):
        if self._reader:
            self._reader.close()
            self._reader = None

    def __del__(self):
        self.close()
