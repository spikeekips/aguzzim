# Copyright (C) 2006, Nuxeo SAS <http://www.nuxeo.com>
# Author: Julien Anguenot <ja@nuxeo.com>
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-13
"""RSS adapter

$Id: adapter.py,v 1.1 2006/08/23 06:38:37 spike Exp $
"""

import cElementTree as etree

#import zope.interface

from nokcene.rss.interfaces import IResultSet
from nokcene.rss.interfaces import IPythonResultSet

class PythonResultSet(object):
    """Transform a IResultSet to IPythonResultSet
    """

    #zope.interface.implements(IPythonResultSet)

    __used_for__ = IResultSet

    def __init__(self, resultset):
        self._elt = etree.XML(resultset.getStream())
        self._results = ()

    def getResults(self):
        for item in self._getItems():
            res = {}
            res[unicode('uid')] = unicode(self._getUidFor(item))
            for field in self._getFieldsFor(item):
                k = unicode(field.attrib['id'])
                if k == unicode('uid'):
                    continue
                v = unicode(field.text)
                if res.has_key(k):
                    if not isinstance(res[k], list):
                        res[k] = [res[k], v]
                    else:
                        old = res[k]
                        old.append(v)
                        res[k] = old
                else:
                    res[k] = v
            self._results += (res,)
        return (self._results, self._getNbItems())

    def _getItems(self):
        return tuple(
            self._elt.getiterator("{http://backend.userland.com/rss2}item"))

    def _getNbItems(self):
        nb_items = 0
        e = self._elt.find("{http://namespaces.nuxeo.org/nxlucene/}nbitems")
        if e is not None:
            try:
                nb_items = int(e.text)
            except TypeError:
                pass
        return nb_items            

    def _getFieldsFor(self, fields_node):
        return fields_node.getiterator(
            "{http://namespaces.nuxeo.org/nxlucene/}field")

    def _getUidFor(self, item_node):
        return item_node.find(
            "{http://backend.userland.com/rss2}guid").text
        
