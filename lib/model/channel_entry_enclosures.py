# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sqlobject
import config

class ChannelEntryEnclosures (sqlobject.SQLObject) :
	_connection = config.database["entry"]

	channelID		= sqlobject.IntCol(notNone=False)
	entryID			= sqlobject.IntCol(notNone=False)
	href				= sqlobject.StringCol(default="", notNone=True)
	length			= sqlobject.IntCol(notNone=False)
	type				= sqlobject.StringCol(default="", notNone=False)

	index0			= sqlobject.DatabaseIndex({"column": "entryID", })
	index1			= sqlobject.DatabaseIndex({"column": "type", "length": 80})
	index2			= sqlobject.DatabaseIndex({"column": "href", "length": 80})


if __name__ == "__main__" :
	import sys
	print ChannelEntryEnclosures.createTable()

"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""
