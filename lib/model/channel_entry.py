# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sqlobject
import database
import config
import channel_entry_enclosures

class PagingChannelEntry :

	def __init__ (self, obj_model_entry) :
		self.obj_model_entry = obj_model_entry

	def get_list (self, **args) :
		__limit = 10
		__offset = 0
		__where_clause = sqlobject.AND(1==1)

		if args.has_key("limit") : __limit = args["limit"]
		if args.has_key("offset") : __offset = args["offset"]
		if args.has_key("where_clause") :
			__where_clause = args["where_clause"]

		return list( \
			self.obj_model_entry.select( \
				__where_clause, \
				orderBy=self.obj_model_entry.q.id, \
				reversed=True\
			)[__offset:__offset + __limit] \
		)

	def get_count (self, **args) :
		return self.obj_model_entry.select().count()

class ChannelEntry (sqlobject.SQLObject) :
	_connection = config.database["entry"]

	channelID		= sqlobject.IntCol(notNone=True)
	author			= sqlobject.UnicodeCol(default="", notNone=False)
	uid				= sqlobject.UnicodeCol(default="", notNone=False)
	link				= sqlobject.UnicodeCol(default="", notNone=True)
	links				= sqlobject.UnicodeCol(default="", notNone=False)
	publisher		= sqlobject.UnicodeCol(default="", notNone=False)
	subtitle			= sqlobject.UnicodeCol(default="", notNone=False)
	summary			= sqlobject.UnicodeCol(default="", notNone=False)
	timeUpdated		= sqlobject.DateTimeCol(default=None, notNone=False)
	title				= sqlobject.UnicodeCol(default="", notNone=False)
	tags				= sqlobject.UnicodeCol(default="", notNone=False)

	index0			= sqlobject.DatabaseIndex({'column': "uid", 'length': 80})
	index1			= sqlobject.DatabaseIndex({'column': "link", 'length': 80})
	index2			= sqlobject.DatabaseIndex({'column': "timeUpdated"})
	index3			= sqlobject.DatabaseIndex({'column': "title", "length": 80})

	def getEnclosures (self) :
		__e = channel_entry_enclosures.ChannelEntryEnclosures
		return __e.select( \
			sqlobject.AND( \
				__e.q.channelID == self.channelID, \
				__e.q.entryID == self.id \
			) \
		)
		
SKEL_TABLENAME_ENTRIES = "channel_entry_%s"
def get_entry (id_channel) :
	__model_entry = get_entry_model(SKEL_TABLENAME_ENTRIES % id_channel)
	__model_entry.createTable( \
		ifNotExists=True, \
		connection=database.get_connection(config.database["entry"]) \
	)

	return __model_entry

def get_entry_model (table_name) :
	return database.get_model(database.get_table("channel_entry"), table_name)


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""
