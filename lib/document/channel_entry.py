# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from util import datetime__

def get_latest_posts (old_data, new_item_list) :
	"""
	* old data is the latest item from the DB
	* and new_item_list must be ordered by updated_parsed DESCENDENTLY.
	"""
	__list = list()
	__n = 0
	for i in new_item_list :
		"""
		NOTE: Find out which post is newly updated one.
			1. match 'uid'
			1. match 'title'
			2. match 'updated_parsed'
		We consider this post as same post.
		"""
		# get uid

		__uid = None
		if i.has_key("id") : __uid = i["id"]
		if i.has_key("gid") : __uid = i["gid"]
		if i.has_key("uid") : __uid = i["uid"]

		if __uid and old_data.uid.strip() : # if guid,
			if __uid == old_data.uid :
				break

		if i.has_key("title") and old_data.title.strip() and \
				i.has_key("link") and old_data.link.strip() : # if link,

			if i["link"] == old_data.link and \
					i["title"] == old_data.title :

				if i.has_key("updated_parsed") and i.updated_parsed : # if date,
					if datetime__.get_datetime(i.updated_parsed).timetuple() == \
							old_data.timeUpdated.timetuple() :
						break
				else : # if 'updated_parser' does not exits, compare with only 'title'
					break

		#__list[__n] = i
		__list.append(i)
		__n += 1

	# sort reversely.
	#__l = __list.items()

	#return [i[1] for i in __l]
	return __list



"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "channel_entry.py"


