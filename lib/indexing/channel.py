# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.utils.html import strip_tags
from func import reencode as __encode

def serialize (obj) :
	"""
	data is,
		dict
		{
			"uid" : uid,
			"data" : [(field name, data_type, value, is store, is tokenized, ), ... ],
		}
	"""

	try :
		__uid__ = __encode(obj.id)
	except :
		raise
		return None

	__data = { \
		"uid" : __uid__, \
		"data" : [ \
			("document_type",	"channel",				"text", True, False,	), \
			("id",				__encode(obj.id),		"text", True, False,	), \
			("__uid__",			__encode(__uid__),	"text", True, False,	), \
			("href",				__encode(obj.href),	"path", True, False,	), \
		],
	}

	if obj.language :
		__language = __encode(obj.language)
	else :
		__language = ""

	if obj.link :
		__link = __encode(obj.link)
	else :
		__link = ""

	if obj.subtitle :
		__subtitle = strip_tags(__encode(obj.subtitle))
	else :
		__subtitle = ""

	if obj.title :
		__title = strip_tags(__encode(obj.title))
	else :
		__title = ""

	if obj.version :
		__version = __encode(obj.version)
	else :
		__version = ""

	if obj.encoding :
		__encoding = __encode(obj.encoding)
	else :
		__encoding = ""

	__data["data"].append(("language",	__language,	"text", True, False,	))
	__data["data"].append(("link",		__link,		"path", True, True,	))
	__data["data"].append(("subtitle",	__subtitle,	"text", True, True,	))
	__data["data"].append(("title",		__title,		"text", True, True,	))
	__data["data"].append(("version",	__version,	"text", True, False,	))
	__data["data"].append(("encoding",	__encoding,	"text", True, False,	))

	__data["data"].append(("link__",		__link,		"text", True, True,	))

	return __data

"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "1.py"


