# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


"""
<doc>
  <fields>
	<field id="name" type="text" analyzer="French" is_tokenized="true" is_store="true">
	  The value of the field to index
	</field>
  </fields>
</doc>
"""

TEMPLATE_DOC = \
"""<?xml version="1.0" encoding="utf-8" ?>
<doc>
	<fields>
%s
	</fields>
</doc>"""

TEMPLATE_FIELD = \
"""\t\t<field id="%s" type="%s" analyzer="%s" is_store="%s" is_tokenized="%s">%s</field>"""

from xml.sax.saxutils import escape
import base64

def serialize (which, data, b64=True) :
	# try to find modules
	try :
		__o = __import__("indexing.%s" % which, globals(), locals(), ["", ])
	except Exception, e :
		print e
		return None

	__d = __o.serialize(data)

	if type(__d) is not dict or not __d :
		return None
	elif not __d.has_key("data") :
		return None

	__field = list()
	for i in __d["data"] :
		if not i[1] :
			continue

		__field.append( \
			TEMPLATE_FIELD % \
				(i[0], i[2], "cjk", str(i[3]).lower(), str(i[4]).lower(), escape(i[1])) \
		)

	if len(__field) < 1 :
		return None
	else :
		__o = TEMPLATE_DOC % "\n".join(__field)

		if not b64 :
			return __o
		else :
			return base64.b64encode(__o)


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "1.py"


