# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.utils.html import strip_tags
from func import reencode as __encode

def serialize (obj) :
	try :
		__uid__ = "%s.%s" % (obj.channelID, obj.id)
	except :
		return None

	__data = { \
		"uid" : __uid__, \
		"data" : [ \
			("document_type",	"channel_entry",				"text", True, False,	), \
			("channel",			__encode(obj.channelID),	"text", True, False,	), \
			("__uid__",			__encode(__uid__),			"text", True, False,	), \
			("id",				__encode(obj.id),				"text", True, False,	), \
		],
	}

	if obj.author :
		__author = strip_tags(__encode(obj.author))
	else :
		__author = ""

	if obj.link :
		__link = __encode(obj.link)
	else :
		__link = ""

	if obj.links :
		__links = __encode(obj.links)
	else :
		__links = ""

	if obj.publisher :
		__publisher = strip_tags(__encode(obj.publisher))
	else :
		__publisher = ""

	if obj.summary :
		__summary = strip_tags(__encode(obj.summary))
	else :
		__summary = ""

	if obj.timeUpdated :
		__timeUpdated = __encode(obj.timeUpdated)
	else :
		__timeUpdated = ""

	if obj.title :
		__title = strip_tags(__encode(obj.title))
	else :
		__title = ""

	if obj.tags :
		__tags = strip_tags(__encode(obj.tags))
	else :
		__tags = ""

	__data["data"].append(("author",				__author,		"text", True, True,	))
	__data["data"].append(("link",				__link,			"path", True, False,	))
	#__data["data"].append(("links",				__links,			"path", True, False,	))
	__data["data"].append(("publisher",			__publisher,	"text", True, True,	))
	__data["data"].append(("summary",			__summary,		"text", True, True,	))
	__data["data"].append(("timeUpdated",		__timeUpdated,	"date", True, False,	))
	__data["data"].append(("title",				__title,			"text", True, True,	))
	#__data["data"].append(("tags",				__tags,			"text", True, False,	))

	__data["data"].append(("link__",				__link,			"text", True, False,	))
	#__data["data"].append(("links__",			__links,			"text", True, False,	))
	__data["data"].append(("timeUpdated__",	__timeUpdated,	"text", True, False,	))

	return __data


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "1.py"


