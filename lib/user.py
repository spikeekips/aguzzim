# -*- coding: utf-8 -*-
#	Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#	   This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
. save the user profile and setting in main.user_profile.
. save the user channel info in main.channel_user
. save the entry, which was saved by user in main.entry_user
. save the memo, which was edited by user in main.memo_user

main.user_profile
	id_user
	username

main.channel_user
	id_user
	id_channel

main.entry_user
	id_user
	id_channel
	id_entry

main.memo_user
	id_user
	memo
	related_to : from_channel, from_entry, alone
		{
			"related_to" : "from_channel",
			"data" : id_channel,
		} => pickling(serilizing)
"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "views.py"


