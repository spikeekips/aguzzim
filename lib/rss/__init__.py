# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import time, datetime
import config
import model

def is_updated (interval, obj_channel) :
	#obj_channel  = get_channel(href)
	if not obj_channel :
		return False

	if not obj_channel.status.timeUpdated :
		return False
	elif obj_channel.status.timeUpdated and \
			obj_channel.status.timeUpdated > \
				datetime.datetime( \
					*time.localtime(time.time() - (interval * 60))[:-2] \
				) :
		return True

	else :
		return False

def exists_href_in_feed (href) :
	if len(list(model.Channel.select(model.Channel.q.href == href))) > 0 :
		return True

	return False

def get_channel (href) :
	__o = model.Channel.select(model.Channel.q.href == href)
	__o = list(__o)
	if len(__o) > 0 :
		return __o[0]

	return None

def get_status (id_channel) :
	__o = model.Status.select(model.Status.q.channelID == id_channel)
	__o = list(__o)
	if len(__o) > 0 :
		return __o[0]

	return None

	


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "__init__.py"


