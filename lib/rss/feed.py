# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from twisted.internet import defer
from rss import parser
from http import pageGetterTwisted

EXCEPTION_ALLOWED_IN_FEEDPARSER = \
	[ \
		parser.CharacterEncodingOverride, \
	]

class Feed :
	href = None

	xml_raw = None
	xml_parsed = None

	def __init__ (self, href) :
		self.href = str(href)

	def get (self, is_parsed=True) :
		__d = defer.maybeDeferred( \
			pageGetterTwisted.get_page, str(self.href))

		__d.addCallback(self.__cb_Parse, is_parsed)
		__d.addErrback(self.__eb_Parse)

		return __d

	def __cb_Parse (self, retval, is_parsed) :
		(factory, xml, ) = retval
		self.xml_raw = xml

		if not is_parsed :
			return xml
		else :
			parsed = parser.parse(xml)
			if parsed.bozo != 0 and parsed.has_key("bozo_exception") and \
					parsed.bozo_exception.__class__ not in EXCEPTION_ALLOWED_IN_FEEDPARSER:
				#raise parsed.bozo_exception
				return None

			parsed.status = factory.status
			self.xml_parsed = parsed

			return parsed

	def __eb_Parse (self, f) :
		self.close()
		f.raiseException()

	def close (self) :
		pass


"""
Description
-----------

ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "feed.py"

