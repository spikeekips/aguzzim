# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

def encode (s, _from="euc-kr", _to="utf-8") :
	try :
		return unicode(s, _from).encode(to)
	except :	
		return s

def reencode (data_unicode, toEncoding="utf8") :
	try :
		if type(data_unicode) is not unicode :
			return str(data_unicode)
		else :
			try :
				return data_unicode.encode(toEncoding)
			except Exception, e :
				return str(data_unicode)
	except Exception, e :
		return data_unicode

import string
def camelize (word) :
	return string.capwords(word, "_").replace("_", "")


import sha, random
def get_unique_id () :
	return sha.new(str(random.random())).hexdigest()

import os
def write_pid (file_pid) :
	fd = file(file_pid, "w")
	fd.write(str(os.getpid()))
	fd.close()

def split_with_field (s, fields=["\"", "'", ]) :
	__d = str()
	__is_qoute = False
	__new = list()
	for i in s.split(" ") :
		i = i.strip()

		if __is_qoute :
			__d += " " + i
			if i[-1] in fields :
				__is_qoute = False
				__new.append(__d)
				__d = str()
			continue

		if not i :
			continue

		if i[0] in fields :
			__is_qoute = True
			__d += i
		else :
			__new.append(i)

	return __new

from xml.sax import saxutils
def quote_xml_attr (s) :
	return saxutils.quoteattr(s)

"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "func.py"


