# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Specific logging configuration
"""

import logging
from logging.handlers import RotatingFileHandler

format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"

def initLog(level, file_, maxBytes=10000000, backupCount=5):
	"""Initialize a Rotating File Handler.
	"""
	# XXX make maxBytes and backupCount configurable
	handler = RotatingFileHandler( \
		file_, maxBytes=maxBytes, backupCount=backupCount)
	formatter = logging.Formatter(format)
	handler.setFormatter(formatter)
	logging.getLogger().addHandler(handler)
	logging.getLogger().setLevel(getattr(logging, level))


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "logger.py"



