# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import time, datetime

class TZ (datetime.tzinfo) :
	def utcoffset (self, dt) :
		return datetime.timedelta(seconds=time.timezone)

def now (with_tz=False, diff_hour=0, diff_minute=0, diff_second=0) :
	__diff = 0
	if diff_hour :
		__diff += diff_hour * 60 * 60

	if diff_minute :
		__diff += diff_minute * 60

	if diff_second :
		__diff += diff_second

	if with_tz :
		__args = time.localtime(time.time() - __diff)[:-2] + (TZ(), )
	else :
		__args = time.localtime(time.time() - __diff)[:-2]

	return datetime.datetime(*(__args))

def get_datetime (time_tuple=tuple()) :
	#__args = time_tuple[:-2] + (TZ(), )
	return datetime.datetime(*time_tuple[:-2])

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "datetime__.py"


