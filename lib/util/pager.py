# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.core.paginatorNEW import ObjectPaginator 

class Default (ObjectPaginator) :

	print_page_current = 0
	print_page_next = 0
	print_page_previous = 0

	page_next = 0
	page_previous = 0

	#def __init__(self, module, args, num_per_page, count_method="get_count", list_method="get_list"):
	#ObjectPaginator.__init__(self, module, args, num_per_page, count_method, list_method)

	def get_page (self, page_number) :
		__retval = ObjectPaginator.get_page(self, page_number)

		try :
			page_number = int(page_number)
		except :
			page_number = 0

		self.page_next = page_number + 1
		self.page_previous = page_number - 1

		self.print_page_current = page_number + 1
		self.print_page_next = self.page_next + 1
		self.print_page_previous = self.page_previous + 1

		if not self.has_next_page(page_number) :
			self.print_page_next = None
			self.page_next = None

		if not self.has_previous_page(page_number) :
			self.print_page_previous = None
			self.page_previous = None

		return __retval


__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "views.py"


