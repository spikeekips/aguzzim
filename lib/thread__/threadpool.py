# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from time import sleep
import threading
from thread__ import __default__

logger = __default__.logger

class Thread (__default__.Thread)
	pass

class ThreadPool (__default__.ThreadPool) :
	pass

class ThreadPoolThread (threading.Thread) :

	""" Pooled thread class. """

	threadSleepTime = 0.1

	def __init__(self, pool) :

		""" Initialize the thread and remember the pool. """

		threading.Thread.__init__(self)
		self.__pool = pool
		self.__isDying = False

	def run(self) :

		""" Until told to quit, retrieve the next task and execute
		it, calling the callback if any.  """

		while self.__isDying == False :
			cmd, args, callback = self.__pool.getNextTask()
			# If there's nothing to do, just sleep a bit
			if cmd is None :
				sleep(ThreadPoolThread.threadSleepTime)
			elif callback is None :
				logger.info(
					"Worker thread %s starts execution ...." % self.getName())
				cmd(*args)
				logger.info(
					"Worker thread %s execution is done ...." % self.getName())
			else :
				logger.info(
					"Worker thread %s starts execution ...." % self.getName())
				callback(cmd(*args))
				logger.info(
					"Worker thread %s execution is done ...." % self.getName())

	def goAway(self) :

		""" Exit the run loop next time through."""

		self.__isDying = True


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "threadpool.py"


