# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

class Pager (dict) :

	def __init__ (self, resultSet, limit, current_page=1) :
		self["total"] = len(list(resultSet))
		self.__limit = limit
		self["current_page"] = current_page

		# calculate
		__n_page = int(self["total"] / self.__limit)
		if self["total"] % self.__limit :
			__n_page += 1

		self["number_of_page"] = __n_page

		self.prev = self.getPrev()
		self.next = self.getNext()

	def getPrev (self) :
		__n = self["current_page"] - 1
		if __n < 1 :
			return None

		return __n

	def getNext (self) :
		__n = self["current_page"] + 1
		if __n > self["number_of_page"] :
			return None

		return __n

def cut_string (s, max_width, delm="<br>") :
	if len(s) <= max_width :
		return s
	else :
		__a = list()
		__b = s
		while 1 :
			if not __b : break
			__a.append(__b[:max_width])
			__b = __b[max_width:]

		return "<br>".join(__a)



"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "template.py"


