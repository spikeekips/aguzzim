# -*- coding: utf-8 -*-
#	Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#	   This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from twisted.python import failure
from twisted.internet import reactor, defer
from twisted.web import client
import time, rfc822, sys

import http

class HTTPPageGetterTwisted (client.HTTPPageGetter) :

	def handleStatus_200 (self) :
		client.HTTPPageGetter.handleStatus_200(self)

		# If we"re good, try recording the last-modified header
		if self.headers.has_key("last-modified") :
			self.factory.lastModified(self.headers["last-modified"][0])

	def handleStatus_304 (self) :
		# Close connection
		self.factory.notModified()
		self.transport.loseConnection()

class HTTPClientFactory (client.HTTPClientFactory) :

	protocol = HTTPPageGetterTwisted
	TIME_LAST_MODIFIED = None

	def __init__(self, \
			url, method='GET', postdata=None, headers=None, \
			agent="Spike's Agent", timeout=0, cookies=None, \
			followRedirect=1 \
		) :

		client.HTTPClientFactory.__init__(self, \
			url, method, postdata, headers, \
			agent, timeout, cookies, followRedirect \
		)

	def lastModified (self, modtime) :
		try :
			self.TIME_LAST_MODIFIED = \
				time.mktime(rfc822.parsedate(modtime))
		except :
			self.TIME_LAST_MODIFIED = None

	def notModified (self) :
		self.TIME_LAST_MODIFIED = None

		if self.waiting :
			self.waiting = 0

def get_page (url, last_modified=None, agent="Spike's Agent", timeout=10) :
	try :
		url = str(url)
	except :
		return None

	headers = dict()
	if last_modified :
		headers.update({"last-modified": rfc822.formatdate(time.time()), })

	def start_getter (url, headers) :
		scheme, host, port, path = http.parse_url(url)
		__url = "%s://%s:%s%s" % (scheme, host, port, path, )

		try :
			__factory = HTTPClientFactory( \
				__url, \
				timeout=timeout, \
				agent=agent, \
				headers=headers, \
			)
		except Exception, e :
			print "Connection: HTTPFactory starting error : %s" % e
			return failure.Failure(e)

		if scheme != "http" :
			return failure.Failure(RuntimeError("Only http scheme is available."))

		reactor.connectTCP(host, port, __factory)

		return __factory

	def done (retval) :
		return (factory, retval)

	def notDone (f) :	
		return (factory, f)

	factory = start_getter(url, headers)

	if isinstance(factory, failure.Failure) :
		factory.raiseException()
	else :
		factory.deferred.addCallback(done)
		factory.deferred.addErrback(notDone)

		return factory.deferred

if __name__ == "__main__" :
	def donedone (retval) :
		(factory, page, ) = retval

		print [page, ]

	#url = "http://groups.google.com/group/django-users/feed/rss_v2_0_topics.xml?num=50"
	url = sys.argv[1]
	get_page(url).addCallback(donedone)

	reactor.run()


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "Requester.py"


