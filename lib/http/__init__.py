# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from twisted.web import http
STATUS_CODE_SUCCESS = 200
STATUS_CODE_UNRESOLVED = 10
ERROR_DNS_LOOKUP = -200
ERROR_TIMEOUT = 11
ERROR_CONNECTION_REFUSED = 111
ERROR_CONNECT = 113
ERROR_BAD_XML = -300
ERROR_RUNTIME = -400
ERROR_DOCUMENT_IS_EMPTY = -500

STATUS_CODE_ENABLED = [ \
	None, \
	ERROR_RUNTIME, \
	ERROR_BAD_XML, \
	STATUS_CODE_UNRESOLVED, \
	ERROR_TIMEOUT, \
	http.OK, \
	http.CREATED, \
	http.ACCEPTED, \
	http.NON_AUTHORITATIVE_INFORMATION, \
	http.NO_CONTENT, \
	http.RESET_CONTENT, \
	http.PARTIAL_CONTENT, \
	http.MULTIPLE_CHOICE, \
	http.MOVED_PERMANENTLY, \
	http.FOUND, \
	http.SEE_OTHER, \
	http.NOT_MODIFIED, \
	http.USE_PROXY, \
	http.TEMPORARY_REDIRECT, \
	http.REQUEST_TIMEOUT, \
	http.GATEWAY_TIMEOUT, \
]

##################################################
# FUNCTIONS
def isAjaxRequest (request) :
	if request.environ.has_key("HTTP_X_REQUESTED_WITH") and \
			request.environ["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest" :
		return True

	return False

from django.utils.httpwrappers import HttpResponse
def exitWithStatus (status_code) :
	__h = HttpResponse("")
	__h.status_code = status_code
	return __h

import urlparse, urllib
from http.pageGetterBasic import get_page
def validate_url (url, strict=False) :
	__url = url
	(__scheme, __netloc, __path, __params, __query, __fragment, ) = \
		urlparse.urlparse(__url)

	if not __scheme and not __scheme.startswith("http") :
		__url = "http://%s" % __url

	(__scheme, __netloc, __path, __params, __query, __fragment, ) = \
		urlparse.urlparse(__url)

	if not (__scheme and __netloc) :
		return None

	try :
		u = get_page(__url)
	except Exception, e :
		if strict :
			raise
		else :
			return None
	else :
		return __url

def parse_url (url, defaultPort=None) :
	parsed = urlparse.urlparse(url)
	scheme = parsed[0]
	path = urlparse.urlunparse(('','')+parsed[2:])
	if defaultPort is None:
		if scheme == 'https':
			defaultPort = 443
		else:
			defaultPort = 80
	host, port = parsed[1], defaultPort
	if ':' in host:
		host, port = host.split(':')
		port = int(port)

	return (scheme, host, port, path, )

def fixurl (url) :
	# re-generate url path.
	__host_and_query = urllib.splitquery(url)

	__value = list()
	for i in __host_and_query[1].split("&") :
		__key_and_value = urllib.splitvalue(i)
		__a = "%s=%s" % ( \
			__key_and_value[0], \
			urllib.quote(__key_and_value[1]), \
		)

		__value.append(__a)

	__url = "%s?%s" % (__host_and_query[0], "&".join(__value), )

	return __url


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "__init__.py"


