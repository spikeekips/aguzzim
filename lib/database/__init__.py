# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sqlobject
import func
import config

def get_connection (uri) :
	__conn = sqlobject.connectionForURI(uri)
	#__conn.debug = True
	#__conn.makeConnection()

	#__conn_raw = __conn.getConnection()
	#__conn_raw.set_character_set("utf8")
	#__conn_raw.cursor().execute("set character set utf8")
	#__conn_raw.cursor().execute("set character_set_connection = 'utf8'")

	return __conn

import new
def get_model (obj_model, table_name) :
	try :
		return new.classobj(table_name, (obj_model,), dict())
	except ValueError :
		return sqlobject.classregistry.findClass(table_name)

def get_table (tablename) :
	try :
		__table = __import__("model.%s" % tablename, None, None, "model")
	except AttributeError :
		return None
	else :
		try :
			return getattr(__table, func.camelize(tablename))
		except AttributeError :
			return None

def get_entry_model (table_name) :
	return get_model(get_table("channel_entry"), table_name)



"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "__init__.py"


