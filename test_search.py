# -*- coding: utf-8 -*-
import sys, xmlrpclib, pprint
import config

__xml = u"""<?xml version="1.0" encoding="utf-8"?>
<search>
	<return_fields>
		<field>title</field>
		<field>summary</field>
		<field>timeUpdated</field>
		<field>author</field>
	</return_fields>
	<fields>
		<field condition="AND">
			<field id="document_type" value="channel_entry" usage=""
					type="keyword" analyzer="cjk" />
			<field condition="OR">
				<field id="title" value="기덕 AND 소통" usage="" type="" analyzer="cjk" />
				<field id="summary" value="기덕 AND 소통" usage="" type="" analyzer="cjk" />
			</field>
		</field>
	</fields>
	<sort>
		<sort-on>uid</sort-on>
		<sort-order>reverse</sort-order>
		<sort-limit>15</sort-limit>
		<operator>OR</operator>
	</sort>
	<batch start="0" size="15"/>
</search>
"""

from nokcene import core
from nokcene import xmlquery_new
s = core.NokceneServer(config.nokcene_directory_index)
(__r, __s, __o, ) = xmlquery_new.xml_to_args(__xml)
a = s.searchQuery(return_fields=__r, search_fields=__s, search_options=__o)
print a
sys.exit()

__xml = u"""<?xml version="1.0" encoding="utf-8"?>
<search>
	<return_fields>
		<field>title</field>
		<field>__uid__</field>
		<field>link__</field>
		<field>summary</field>
		<field>timeUpdated</field>
		<field>author</field>
	</return_fields>
	<fields>
		<field condition="AND">
			<field id="document_type" value="channel_entry" usage=""
				type="keyword" analyzer="cjk" />
			<field condition="OR">
				<field id="summary" value="SUMMARY AND SUMMARY0" usage="" type=""
					analyzer="cjk" />
				<field id="author" value="AUTHOR" usage="" type=""
					analyzer="cjk" />
			</field>
			<field id="timeUpdated" value="2006.07.10 00:00:00" usage="range:max"
				type="date" analyzer="cjk" />
		</field>
	</fields>
	<sort>
		<sort-on>uid</sort-on>
		<sort-order>reverse</sort-order>
		<sort-limit>15</sort-limit>
		<operator>OR</operator>
	</sort>
	<batch start="0" size="15"/>
</search>
"""

import nokcene.xmlquery_new
(__r, __s, __o, ) = nokcene.xmlquery_new.xml_to_args(__xml)

__k = nokcene.xmlquery_new.args_to_xml( \
	return_fields=__r, search_fields=__s, search_options=__o, )
__args = nokcene.xmlquery_new.xml_to_args(__k)

print nokcene.xmlquery_new.args_to_xml(*__args)

sys.exit()

pprint.pprint(__arg)
__query = nokcene.xmlquery_new.search_field_to_query(__arg)

print [__query, ]

sys.exit(1)

import nokcene.result

return_fields=(
	"title",
	"__uid__",
	"link__",
	"summary",
	"timeUpdated",
	"author",
)
search_fields=( \
	{
		"id" : "document_type",
		"type" : "keyword",
		"value": "channel_entry",
		"usage" : "",
		"condition" : "AND",
	},
	( \
		{
			"id" : "title",
			"type" : "",
			"value": "기덕 AND 소통",
			"usage" : "",
			"condition" : "OR",
		},
		{
			"id" : "summary",
			"type" : "",
			"value": "기덕 AND 소통",
			"usage" : "",
			"condition" : "OR",
		},
	)

)
search_options = { \
	"sort-on" : "uid",
	"sort-order" : "reverse",
	"start" : 0,
	"size" : 15,
	"operator" : "",
}

import qqqq

__query = qqqq.generate_query( \
	search_fields, \
	search_options \
)

print "|||||||||||||||||||||||||||||||||||||"
print [__query]

sys.exit()
__xml = nokcene.result.generate_xml_query( \
	return_fields=return_fields, \
	search_fields=search_fields, \
	search_options=search_options \
)
#print __xml

(__result, __num, ) = nokcene.result.get_result(__xml)

for i in __result :
	print i.get("title")


sys.exit()
from PyLucene import CJKAnalyzer, SimpleAnalyzer, Document, TermQuery, QueryParser, IndexSearcher, Term

searcher = IndexSearcher(config.nokcene_directory_index)

__q = ""
__q = "+document_type:channel_entry +(title:기덕 title:소통)"

query = QueryParser("", CJKAnalyzer()).parse(__q)
print [query, ]

hits = searcher.search(query)

for i in hits :
	print i.get("author"), " : ", i.get("title")



