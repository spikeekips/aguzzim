# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os, sys

from model.channel import Channel as model_channel
from model import channel_entry
import config
import document.channel
import indexing
from nokcene import core, xmlquery
import indexing
from document.channel import get_uid_channel, get_uid_channel_entry

###############################################3
# channel & channel_entry
s = core.NokceneServer(config.nokcene_directory_index)
for i in list(model_channel.select()) :
	print i.id
	__entry = channel_entry.get_entry(i.id)
	__list = list(__entry.select())
	if len(__list) < 1 :
		continue

	print "indexing, channel %s : %s" % (i.id, i.title)
	__iquery = xmlquery.XMLQuery(indexing.serialize("channel", i, False))
	s.indexDocument(get_uid_channel(i), __iquery)

	for j in __list :
		print "\t> indexing, channel_entry %s : %s" % (j.id, j.title)
		__iquery = xmlquery.XMLQuery(indexing.serialize("channel_entry", j, False))
		s.indexDocument(get_uid_channel_entry(j), __iquery)


sys.exit()
"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "test_reindex.py"



