# -*- coding: utf-8 -*-
import sys, xmlrpclib, pprint
import config, func
from nokcene import core
from nokcene import xmlquery_new
from nokcene import result

__o = "limit* url*"
__s = " AND ".join(func.split_with_field(__o))

print __s

return_fields=(
	"title",
	"__uid__",
	"link__",
	"summary",
	"timeUpdated",
	"author",
)
search_fields=( \
	(
		"AND",
		{
			"id" : "document_type",
			"type" : "keyword",
			"value": "channel_entry",
			"usage" : "",
			"condition" : "AND",
		},
		(
			"OR",
			{
				"id" : "title",
				"type" : "",
				"value": "%s" % __s,
				"usage" : "",
			},
			{
				"id" : "summary",
				"type" : "",
				"value": "%s" % __s,
				"usage" : "",
			},
		),
		
	),
)
search_options = { \
	"sort-on" : "uid",
	"sort-order" : "reverse",
	"start" : 0,
	"size" : 5,
	"operator" : "OR",
}

__xml = xmlquery_new.args_to_xml( \
	return_fields=return_fields, \
	search_fields=search_fields, \
	search_options=search_options \
)

s = core.NokceneServer(config.nokcene_directory_index)
(__r, __s, __o, ) = xmlquery_new.xml_to_args(__xml)

__result_xml = s.searchQuery(return_fields=__r, search_fields=__s, search_options=__o)
(__result, num, ) = result.generate_result(__result_xml)
for i in __result :
	print "====================================================="
	print i["title"]
	print "-----------------------------------------------------"
	print i["summary"][:200]


sys.exit()


