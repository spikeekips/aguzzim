/* ************************************************** */
/* ETC FUNCTION */
function findChildrenByClassName (id_element, className)
{
   var __children= new Array();
   $A($(id_element).childNodes).each(
      function(e)
      {
         if (e.className && e.className.toString() == className)
         {
            __children.push(e);
         }
      }
   );

   return __children;
}

function findChildren (id_element, tagName)
{
   var __children= new Array();
   $A($(id_element).childNodes).each(
      function(e)
      {
         if (e.tagName && e.tagName.toUpperCase()== tagName)
         {
            __children.push(e);
         }
      }
   );

   return __children;
}

function trimString(sInString) {
  sInString = sInString.replace( /^\s+/g, "" ); // strip leading
  return sInString.replace( /\s+$/g, "" ); // strip trailing
}

function isExists (id_element)
{
	if (! document.getElementById(id_element))
	{
		return false;
	} else {
		return true;
	}
}

function isMSIE ()
{
	if(/MSIE/.test(navigator.userAgent))
	{
		return true;
	}

	return false;
}

function getOffsetRight (id_element)
{
	//var __width = document.body.offsetWidth;
	var __width = window.innerWidth;
	var __width_box = $(id_element).offsetWidth;
	var __left = $(id_element).offsetLeft;

	if (__width < (__left + __width_box))
	{
		return __width;
	}

	return __width - (__left + __width_box);
}

var __list_element_for_menu = ["block_channel", "block_channel_entry"];
function changeMenu (element_to_show)
{
	var __id_element_to_hide = null;
	for (var i = 0; i < __list_element_for_menu.length; i++)
	{
		if (__list_element_for_menu[i] == element_to_show) continue;
		if ($(__list_element_for_menu[i]).style.display == "none") continue;
		__id_element_to_hide = __list_element_for_menu[i];
	}

	new Effect.DropOut(
		__id_element_to_hide,
		{
			afterFinish: function (e)
			{
				new Effect.Appear(
					element_to_show,
					{
					}
				);
			}
		}
	);

	var __ids = findChildrenByClassName("block_menu", "select");
	for (var i = 0; i < __ids.length; i++)
	{
		__ids[i].className = "unselect";
	}

	$("li_" + element_to_show).className = "select";
}

function getHeightAvailable ()
{
	var __height_document = window.innerHeight;
	var __top = $("agu_body").offsetTop;
	var __top_copyright = $("block_copyright").offsetTop;

	return (__height_document - __top - (__height_document - __top_copyright)) - 10;
}

function setHeightOfBlock (id_element)
{
	$(id_element).style.height = (getHeightAvailable() - 0) + "px";
}

function updateChannelList (type)
{
	Element.update("block_list_channel", "");
	new Ajax.Updater(
		"block_list_channel",
		"/channel/getChannelList/",
		{
			parameters: "type=" + type,
			asyncronous: true,
			evalScripts: true,
			onComplete: function(request)
			{
				//alert("findme");
			}
		}
	);
}

function showEntriesBoxChannelEntry ()
{
	if (! isExists("block_shortcut_link_box_channel_entry"))
	{
		return false;
	}

	if ($("block_shortcut_link_box_channel_entry").style.display == "none")
	{
		$("block_shortcut_link_box_channel_entry").style.right =
			(getOffsetRight("block_list_channel_entry") - 10) + "px";
		var __top = $("link_entries_channel_entry").offsetTop;
		$("block_shortcut_link_box_channel_entry").style.top =
			(__top + $("link_entries_channel_entry").offsetHeight + 1) + "px";

		Element.setOpacity("block_shortcut_link_box_channel_entry", 0.7);
		Element.show("block_shortcut_link_box_channel_entry");
	} else {
		Element.hide("block_shortcut_link_box_channel_entry");
	}
}


