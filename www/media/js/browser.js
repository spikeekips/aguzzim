function setLayoutBrowser ()
{
	var __width_window = window.innerWidth;
	var __height_window = window.innerHeight - 20;

	var __height_header = $("block_header").offsetHeight;
	var __height_content = __height_window - __height_header;
	$("block_channel_list").style.height = __height_content + "px";
	$("block_channel_content").style.height = __height_content + "px";

	var __height_browser_channel_action =
		$("block_browser_channel_action").offsetHeight;

	var __height_channel_block =
		__height_content - __height_browser_channel_action;
	var __height_browser_channel_info = parseInt(__height_channel_block * (3/10));
	var __height_browser_channel_list =
		__height_channel_block - __height_browser_channel_info - 10;
	$("block_browser_channel_list").style.height =
		__height_browser_channel_list + "px";
	$("block_browser_channel_info").style.height =
		__height_browser_channel_info + "px";

	$("block_channel_content_item_list").style.height = __height_content + "px";
	/*
	$("block_channel_content_item_content").style.height =
		parseInt(__height_content / 2) + "px";
	*/

}

function updateChannelList ()
{
	new Ajax.Updater(
		"block_browser_channel_list",
		"/xml_getChannelList/",
		{
			asyncronous: true,
			evalScripts: true,
			onComplete: function (request)
			{
			}
		}
	);
}

function updateChannelInfo (id_feed)
{
	new Ajax.Updater(
		"block_browser_channel_info",
		"/xml_getChannel/",
		{
			asyncronous: true,
			evalScripts: true,
			parameters: "id_feed=" + id_feed,
			onComplete: function (request)
			{
			}
		}
	);
}

function updateItemList (id_feed)
{
	$("block_channel_content_item_list").scrollTop = 0;
	new Ajax.Updater(
		"block_channel_content_item_list",
		"/xml_getItemList/",
		{
			asyncronous: true,
			evalScripts: true,
			parameters: "id_feed=" + id_feed,
			onComplete: function (request)
			{
			}
		}
	);
}

function updateItemContent (id_feed, id_item)
{
	var __id_item_content =
		"block_item_content_" + id_feed + "_" + id_item;
	var __link_item_title =
		"link_item_title_" + id_feed + "_" + id_item;

	if ($(__id_item_content).style.display != "none")
	{
		Element.hide(__id_item_content);
		return;
	}

	if (ID_ITEM_CONTENT_PREVIOUS && isExists(ID_ITEM_CONTENT_PREVIOUS))
	{
		if (ID_ITEM_CONTENT_PREVIOUS != __id_item_content)
		{
			Element.hide(ID_ITEM_CONTENT_PREVIOUS);
		}
	}

	if ($(__id_item_content).innerHTML)
	{
		Element.show(__id_item_content);
		// scrolling
		$("block_channel_content_item_list").scrollTop =
			$(__id_item_content).offsetTop -
			$(__link_item_title).offsetHeight -
			$("block_channel_content_item_list").offsetTop;
	} else {
		new Ajax.Request(
			"/xml_getItem/",
			{
				asyncronous: true,
				evalScripts: true,
				parameters: "id_feed=" + id_feed + "&id_item=" + id_item,
				onComplete: function (request)
				{
					if (request.status == 200)
					{
						Element.update(__id_item_content, request.responseText);
						Element.show(__id_item_content);
						// scrolling
						$("block_channel_content_item_list").scrollTop =
							$(__id_item_content).offsetTop -
							$(__link_item_title).offsetHeight -
							$("block_channel_content_item_list").offsetTop;

					}
				}
			}
		);
	}

	ID_ITEM_CONTENT_PREVIOUS = __id_item_content;
}

/* GLOBAL VARIABLES */
var ID_ITEM_CONTENT_PREVIOUS = null;
