# -*- coding: utf-8 -*-
#	Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#	   This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.utils.httpwrappers import HttpResponse, HttpResponseRedirect
from django.core.extensions import render_to_response
from django.models.auth import users

import http

def index (request) :
	__user = request.session.get("user", False)
	if not __user :
		return HttpResponseRedirect("/frontpage/login")
	else :
		return HttpResponseRedirect("/channel/")

def login (request) :
	if request.session.get("user", False) :
		del request.session["user"]

	return render_to_response("frontpage/login")

def logout (request) :
	try :
		del request.session["user"]
	except :
		pass

	return HttpResponseRedirect("/frontpage/login")

def checkLogin (request) :
	"""
	@retval is,
		404 : username or password was not given
		405 : username does not exists
		406 : password mis-match
	"""
	__argument = request.POST.copy()

	if not __argument.has_key("username") or \
			not __argument.has_key("password") :
		return http.exitWithStatus(404)

	kwargs = { \
		"username__exact": __argument["username"], \
		"is_active__exact": True \
	}
	try:
		user = users.get_object(**kwargs)
	except users.UserDoesNotExist:
		return http.exitWithStatus(405)

	# check the password and any permission given
	if user.check_password(__argument["password"]):
		request.session["user"] = user
		return http.exitWithStatus(200)
	else :
		return http.exitWithStatus(406)


__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "views.py"


