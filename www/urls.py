from django.conf.urls.defaults import *

import os

urlpatterns = patterns("",
    # Example:
    # (r"^agu/", include("agu.apps.foo.urls.foo")),

	# media, javascript, css, images, etc.
	(r"^__media__/(?P<path>.*)$", \
		"django.views.static.serve", \
			{"document_root": "media"}),

	# favicon.
	(r"^favicon\.ico$", \
		"django.views.static.serve", { \
			"document_root": "media", \
			"path": "/__media__/image/favicon.ico", \
		}
	),

    # Uncomment this for admin:
     (r"^admin/", include("django.contrib.admin.urls.admin")),

)

