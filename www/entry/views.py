# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.utils.httpwrappers import HttpResponse
from django.core.extensions import render_to_response
import simplejson

import model, http
from model import channel, channel_entry

def __serializeChannel (obj_channel) :
	__keys_channel = { \
		"id"           : None, \
		"href"         : None, \
		"language"     : None, \
		"link"         : None, \
		"subtitle"     : None, \
		"title"        : None, \
		"version"      : None, \
		"encoding"     : None, \
	}
	__keys_status = { \
		"title"        : "title_status", \
		"status"       : None, \
		"timeAdded"    : None, \
		"timeUpdated"  : None, \
	}

	__e = dict()
	for i, j in __keys_channel.items() :
		if j :
			__k = j
		else :
			__k = i
		__e.update({__k : unicode(getattr(obj_channel, i)), })

	for i, j in __keys_status.items() :
		if j :
			__k = j
		else :
			__k = i

		__e.update({__k : unicode(getattr(obj_channel.status, i)), })

	return __e

def index (request) :
	return render_to_response( \
		"channel/list_channel", \
	)

def __get_channel_data (id_channel) :
	return channel.Channel.get(id_channel)

def get_channel_info_box (request) :
	__argument = request.POST.copy()
	if not __argument.has_key("id_channel") : return http.exitWithStatus(404)

	__o = __get_channel_data(int(__argument["id_channel"]))
	return render_to_response( \
		"channel/info_channel_box.partial", \
		{
			"channel" : __o, \
		}
	)

def get_channel_data (request) :
	__argument = request.POST.copy()
	if not __argument.has_key("id_channel") : return http.exitWithStatus(404)

	__o = __get_channel_data(int(__argument["id_channel"]))
	return HttpResponse(simplejson.dumps(__serializeChannel(__o)))

def get_channel_list (request) :
	__o = list(channel.Channel.select())

	return render_to_response( \
		"channel/list_channel.partial", \
		{
			"list_channel" : __o, \
		}
	)

def get_channel_entry_list (request) :
	__argument = request.POST.copy()
	if not __argument.has_key("id_channel") : return http.exitWithStatus(404)

	try :
		__channel_entry = channel_entry.get_entry(int(__argument["id_channel"]))
	except Exception, e :
		return http.exitWithStatus(500)

	__o = list(__channel_entry.select(orderBy=__channel_entry.q.id, reversed=True))

	return render_to_response( \
		"channel/list_channel_entry.partial", \
		{
			"list_channel_entry" : __o, \
		}
	)

def get_channel_entry_info (request) :
	__argument = request.POST.copy()
	if not __argument.has_key("id_channel") : return http.exitWithStatus(404)
	if not __argument.has_key("id_entry") : return http.exitWithStatus(404)

	try :
		__channel_entry = channel_entry.get_entry(int(__argument["id_channel"]))
	except Exception, e :
		return http.exitWithStatus(500)

	__o = __channel_entry.get(__argument["id_entry"])

	return render_to_response( \
		"channel/info_channel_entry.partial", \
		{
			"channel_entry" : __o, \
		}
	)

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "views.py"


