# Django settings for agu project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
	("admin", "spikeekips@gmail.com"),
)

MANAGERS = ADMINS

DATABASE_ENGINE = "mysql" # "postgresql", "mysql", "sqlite3" or "ado_mssql".
DATABASE_NAME = "agu"			 # Or path to database file if using sqlite3.
DATABASE_USER = "agu"			 # Not used with sqlite3.
DATABASE_PASSWORD = "agu"		 # Not used with sqlite3.
DATABASE_HOST = "127.0.0.1"			 # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = 3318			 # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. All choices can be found here:
# http://www.postgresql.org/docs/current/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
TIME_ZONE = "Asia/Seoul"

# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
LANGUAGE_CODE = "ko-kr"

SITE_ID = 1

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = "media"

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = ""

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = "/media/"

# Make this unique, and don"t share it with anybody.
SECRET_KEY = "t-iz0ls_(e1o(91rf&8^@f3h3r9+(22h^pg4#ce9tgnajsdut4"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
	"django.core.template.loaders.filesystem.load_template_source",
	"django.core.template.loaders.app_directories.load_template_source",
#	 "django.core.template.loaders.eggs.load_template_source",
)

TEMPLATE_CONTEXT_PROCESSORS = (
	"django.core.context_processors.request",
	"django.core.context_processors.auth",
	"django.core.context_processors.debug",
	"django.core.context_processors.i18n",
)

MIDDLEWARE_CLASSES = (
	"django.middleware.common.CommonMiddleware",
	"django.middleware.sessions.SessionMiddleware",
	"django.middleware.doc.XViewMiddleware",
	"middleware.MethodHandler.handler",
)

ROOT_URLCONF = "www.urls"

TEMPLATE_DIRS = (
	# Put strings here, like "/home/html/django_templates".
	# Always use forward slashes, even on Windows.
	"template", \
)

INSTALLED_APPS = (
	"django.contrib.admin", \
	"django.contrib.auth", \
)

