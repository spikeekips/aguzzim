# -*- coding: utf-8 -*-
#	Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#	   This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.core.exceptions import Http404, ImproperlyConfigured, ViewDoesNotExist
from django.conf import settings
from www.urls import urlpatterns
import os

def get_callback (mod_name, func_name) :
	if not mod_name :
		return None
	try:
		return getattr(__import__(mod_name, '', '', ['']), func_name)
	except ImportError, e:
		raise ViewDoesNotExist, "Could not import %s. Error was: %s" % (mod_name, str(e))
	except AttributeError, e:
		raise ViewDoesNotExist, "Tried %s in module %s. Error was: %s" % (func_name, mod_name, str(e))

class handler :

	def process_request (self, request):
		__path = request.environ["PATH_INFO"]

		# check url patterns
		for i in urlpatterns :
			if i.regex.findall(__path[1:]) :
				return

		__root = os.getcwd().split("/")[-1]
		__paths = [i for i in __path.split("/") if i.strip()]

		if len(__paths) == 2 :
			__mod, __func = "%s.%s.views" % (__root, __paths[0], ), __paths[1]
		elif len(__paths) == 1 :
			__mod, __func = "%s.%s.views" % (__root, __paths[0], ), "index"
		else :
			__mod, __func = "%s.frontpage.views" % __root, "index"

		return get_callback(__mod, __func)(request)


__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "MethodHandler.py"


