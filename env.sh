##################################################################
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##################################################################

AGU_HOME=`pwd`
GCC_HOME=/usr/local/lib/gcc/3.4.4

MYSQL_HOME=$AGU_HOME/db/main
PYTHONPATH=${AGU_HOME}/lib:${PYTHONPATH}
LD_LIBRARY_PATH=${MYSQL_HOME}/lib:${GCC_HOME}/lib:${LD_LIBRARY_PATH}
PATH=${AGU_HOME}/bin:${PATH}

export PYTHONPATH PATH LD_LIBRARY_PATH

