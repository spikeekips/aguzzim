#!/usr/bin/env python2.4

import os
import sys

import config

os.system( \
	"twistd -ny service.py --pidfile=%s -l %s " % \
	( \
		config.zzim_add_file_pid, \
		config.zzim_add_file_log_daemon, \
	) \
)

