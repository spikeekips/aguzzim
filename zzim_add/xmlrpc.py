# -*- coding: utf-8 -*-
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-13
"""
XML-RPC ZZim Add Server adapter
"""

import gc
import logging
from twisted.web import xmlrpc

from document import channel as document_channel

class ZZimAddServer(xmlrpc.XMLRPC) :
	"""XML-RPC server """

	def __init__ (self) :
		xmlrpc.XMLRPC.__init__(self)
		self.log = logging.getLogger("ZZimAddServer")

	def xmlrpc_isExist (self, href) :
		return document_channel.is_exists(href)

	def xmlrpc_addChannel (self, href) :
		self.log.debug( \
			"xmlrpc_updateChannel gc.garbage : %s" % str(gc.garbage))

		return document_channel.Channel(href=href).add()

	def xmlrpc_addChannelByPage (self, href) :
		self.log.debug( \
			"xmlrpc_updateChannel gc.garbage : %s" % str(gc.garbage))

		return document_channel.Channel(href=href).add()


