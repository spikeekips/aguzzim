#!/usr/bin/env python2.4
"""
runner
"""

import gc
import os
import sys
import logging
import threading

from twisted.web import resource
from twisted.web import server

from twisted.application import service
from twisted.application import internet

from logger import initLog
from xmlrpc import ZZimAddServer

from twisted.internet import reactor
reactor.suggestThreadPoolSize(10)

application = service.Application("ZZimAdd")

import config

class Controller (object) :

	def __init__ (self) :

		initLog( \
			config.zzim_add_log_level, config.zzim_add_file_log, \
			config.log["maxbyte"], config.log["backupcount"] \
		)
		self.log = logging.getLogger()

		if config.zzim_add_log_level == "DEBUG" :
			self.log.info("gc.set_debug(gc.DEBUG_LEAK)")
			gc.set_debug(gc.DEBUG_LEAK)

		self._root = resource.Resource()
		self.initializeResources()

	def initializeResources (self) :
		"""Initialize server resources

		We could define SOAP here for instance as well.
		"""

		# Adapt to RPC and register this resource.
		self._root.putChild("RPC2", ZZimAddServer())
		self.log.info("XML-RPC server is listening on port %s..." % config.zzim_add_port)

	def start (self) :
		res = internet.TCPServer(config.zzim_add_port, server.Site(self._root))
		res.setServiceParent(application)

Controller().start()

