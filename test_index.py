# -*- coding: utf-8 -*-
import sys, time
from PyLucene import FSDirectory, IndexWriter, Field, CJKAnalyzer, SimpleAnalyzer, Document, TermQuery, QueryParser, IndexSearcher, Term
import config

from model.channel import Channel as model_channel
from model import channel_entry

dir = FSDirectory.getDirectory(config.nokcene_directory_index, True)
writer = IndexWriter(dir, CJKAnalyzer(), True)
writer.setUseCompoundFile(False)

def get_entries (id_channel) :
	__entry = channel_entry.get_entry(id_channel)
	return list(__entry.select())

def addDocument (entry) :
	doc = Document()
	doc.add(Field("uid", "%s.%d" % (entry.channelID, entry.id), Field.Store.YES, Field.Index.UN_TOKENIZED, ))
	doc.add(Field("document_type", "channel_entry", Field.Store.YES, Field.Index.UN_TOKENIZED, ))
	doc.add(Field("author", entry.author, Field.Store.YES, Field.Index.TOKENIZED, ))
	doc.add(Field("link", entry.link, Field.Store.YES, Field.Index.UN_TOKENIZED, ))
	doc.add(Field("publisher", entry.publisher, Field.Store.YES, Field.Index.TOKENIZED, ))
	doc.add(Field("summary", entry.summary, Field.Store.YES, Field.Index.TOKENIZED, ))
	doc.add(Field("timeUpdated", entry.timeUpdated.strftime("%F %T"), Field.Store.YES, Field.Index.UN_TOKENIZED, ))
	doc.add(Field("title", entry.title, Field.Store.YES, Field.Index.TOKENIZED, ))
	doc.add(Field("tags", entry.tags, Field.Store.YES, Field.Index.UN_TOKENIZED, ))

	doc.add(Field("link__", entry.link, Field.Store.YES, Field.Index.UN_TOKENIZED, ))
	doc.add(Field("links__", entry.links, Field.Store.YES, Field.Index.UN_TOKENIZED, ))
	doc.add(Field("timeUpdated__", str(entry.timeUpdated), Field.Store.YES, Field.Index.UN_TOKENIZED, ))

	writer.addDocument(doc)

__list = get_entries(63)
for i in __list :
	print addDocument(i)

writer.optimize()
writer.close()

sys.exit()

doc = Document()
doc.add(Field("id", "findme", Field.Store.YES, Field.Index.TOKENIZED))
writer.addDocument(doc)

writer.optimize()
writer.close()


