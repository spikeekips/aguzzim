# -*- coding: utf-8 -*-
#    Copyright 2005 Spike^ekipS <spikeekips@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

HOME_AGUZZIM = "/mnt/harddisk/hda6/home/spike/workspace/aguzzim"

#############################################################################33
# AGENT
AGENT_NAME = \
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.1) Gecko/20060209 Debian/1.5.dfsg+1.5.0.1-2 Firefox/1.5.0.1"
TIMEOUT = 15 # Timeout in seconds for the web request

#############################################################################33
# DIRECTORY
directory = dict()
directory["favicon"] = "%s/db/data/favicon" % HOME_AGUZZIM

#############################################################################33
# LOG
log = dict()
log["name"] = "AGU"
log["maxbyte"] = 1024 * 1000 * 10 # 10M
log["backupcount"] = 2
log["directory"] = "%s/log" % HOME_AGUZZIM
log["level"] = "info"

#############################################################################33
# DATABASE
database = dict()
database["main"] = "mysql://agu:agu@127.0.0.1:3316/agu"
database["entry"] = "mysql://agu:agu@127.0.0.1:3317/agu"
database["django"] = "mysql://agu:agu@127.0.0.1:3318/agu"

#############################################################################33
# ZZIM CLIENT
zzim_update_max_thread_number = 10
zzim_update_file_pid = "%s/zzim_update.pid" % (log["directory"], )
zzim_update_file_log_daemon = "%s/zzim_updated.log" % (log["directory"], )
zzim_update_file_log = "%s/zzim_update.log" % (log["directory"], )
zzim_update_log_level = "DEBUG"
zzim_update_update_interval = 5 # minutes

# ZZIM ADD
zzim_add_max_thread_number = 50
zzim_add_port = 2005
zzim_add_file_pid = "%s/zzim_add.pid" % (log["directory"], )
zzim_add_file_log_daemon = "%s/zzim_addd.log" % (log["directory"], )
zzim_add_file_log = "%s/zzim_add.log" % (log["directory"], )
zzim_add_log_level = "INFO"

# NOKCENE
nokcene_directory_index = "%s/db/index" % HOME_AGUZZIM
nokcene_max_thread_number = 50
nokcene_port = 2007
nokcene_file_pid = "%s/nokcene.pid" % (log["directory"], )
nokcene_file_log_daemon = "%s/nokcened.log" % (log["directory"], )
nokcene_file_log = "%s/nokcene.log" % (log["directory"], )
nokcene_log_level = "DEBUG"


"""
Description
-----------


ChangeLog
---------


Usage
-----


"""

__author__ =  "Spike^ekipS <spikeekips@gmail.com>"
__version__=  "0.1"
__nonsense__ = ""

__file__ = "config.py"



