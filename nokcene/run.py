#!/usr/bin/env python2.4
import os, sys
import config

os.system( \
	"twistd -ny service.py --pidfile=%s -l %s " % \
	( \
		config.nokcene_file_pid, \
		config.nokcene_file_log, \
	) \
)

